package cl.ubb.kata.factorial.test;
import static org.junit.Assert.*;
import org.junit.Test;

import cl.ubb.kata.factorial.clase.Fact;


public class FactTest {
	int resultado =0;
	@Test
	public void test() {
		//arrange
		Fact f = new Fact();				
		//act
		resultado=f.calcularFactorial(0);
		//assert
		assertEquals(0,resultado);
	}
	@Test
	public void factorialDe1es1(){
		//arrange
		Fact f = new Fact();		
		//act
		resultado=f.calcularFactorial(1);
		//assert
		assertEquals(1,resultado);
	}
	@Test
	public void factorialDe2es2(){
		//arrange
		Fact f = new Fact();		
		//act
		resultado=f.calcularFactorial(2);
		//assert
		assertEquals(2,resultado);
	}
	@Test
	public void factorialDe3es6(){
		//arrange
		Fact f = new Fact();		
		//act
		resultado=f.calcularFactorial(3);
		//assert
		assertEquals(6,resultado);
	}
	
	@Test
	public void factorialDe4es24(){
		//arrange
		Fact f = new Fact();		
		//act
		resultado=f.calcularFactorial(4);
		//assert
		assertEquals(24,resultado);
	}
	

}
